import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddbookComponent } from './addbook/addbook.component';
import { ListBookComponent } from './list-book/list-book.component';
import { UpdateBookComponent } from './update-book/update-book.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { NotFoundComponentComponent } from './not-found-component/not-found-component.component';
import { ErrorComponent } from './error/error.component';
import { GlobalErrorHandler } from './services/global-error-handler.service';
import { BookService } from './services/book.service';

@NgModule({
  declarations: [
    AppComponent,
    AddbookComponent,
    ListBookComponent,
    UpdateBookComponent,
    BookDetailsComponent,
    NotFoundComponentComponent,
    ErrorComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [BookService,
    { provide: ErrorHandler, useClass: GlobalErrorHandler }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
