import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

import { BookService } from '../services/book.service';
import { Book } from '../Modules/Book';


@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css']
})
export class UpdateBookComponent implements OnInit {

  constructor(
    private bookService: BookService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  public task: Book;
  public index;

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.index = parseInt(params.get('id'), 10);
      this.bookService.getBook(this.index).subscribe(data => {
        this.task = data;
      });
    });
  }
  saveChanges(data) {
    this.bookService.savebook(data, this.index).subscribe(() => {
      this.router.navigate(['/list-book']);
    });
  }

}
