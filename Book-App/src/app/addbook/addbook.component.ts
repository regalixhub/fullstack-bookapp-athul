import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { BookService } from '../services/book.service';
import { Book } from '../Modules/Book';


@Component({
  selector: 'app-addbook',
  templateUrl: './addbook.component.html',
  styleUrls: ['./addbook.component.css']
})
export class AddbookComponent {

  constructor(private route: Router, private bookservice: BookService) { }

  createBook(data: Book) {
    this.bookservice.createBook(data).subscribe(() => {
      this.route.navigate(['list-book']);
    });
  }
}
