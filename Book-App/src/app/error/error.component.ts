import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-error',
  template: `<h2 class="text-danger">Error</h2>
             <p class="text-danger">An error occured !!</p>`
})
export class ErrorComponent {

  constructor() { }


}
