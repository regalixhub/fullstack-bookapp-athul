import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddbookComponent } from './addbook/addbook.component';
import { ListBookComponent } from './list-book/list-book.component';
import { UpdateBookComponent } from './update-book/update-book.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { NotFoundComponentComponent } from './not-found-component/not-found-component.component';
import { ErrorComponent } from './error/error.component';


const routes: Routes = [
  { path: '', redirectTo: 'list-book', pathMatch: 'full' },
  { path: 'add-book', component: AddbookComponent },
  { path: 'list-book', component: ListBookComponent },
  { path: 'update-book/:id', component: UpdateBookComponent },
  { path: 'book-details/:id', component: BookDetailsComponent },
  { path: 'error', component: ErrorComponent },
  { path: '404', component: NotFoundComponentComponent },
  { path: '**', redirectTo: '/404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
