import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

import { BookService } from '../services/book.service';
import { Book } from '../Modules/Book';


@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {

  public index;
  public oneBook: Book;
  public errorMsg;

  constructor(private bookService: BookService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.index = parseInt(params.get('id'), 10);
      this.bookService.getBook(this.index).subscribe(data => {
        this.oneBook = data;
      });
    });

  }
}
